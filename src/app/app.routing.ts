//importar modulos de router en angular
import { ModuleWithProviders} from '@angular/core';
import { Routes , RouterModule } from '@angular/router';

//Importar componentes
import { HomeComponent} from './home/home.component';
import { ExternoComponent } from './externo/externo.component';

// Array de rutas
const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'externo', component: ExternoComponent},
    {path: '**', component: HomeComponent},
    
];

export const appRoutingProviders: any[] = []; //exportar el service
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes); //y el modulo