import { Component, OnInit } from '@angular/core';
import { PeticionesService } from '../service/peticiones.service';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';



@Component({
  selector: 'app-externo',
  templateUrl: './externo.component.html',
  styleUrls: ['./externo.component.css'],
  providers: [PeticionesService]
})
export class ExternoComponent implements OnInit {

    public names: any;
    public nameId: any;

    constructor(

      private _peticionesService: PeticionesService

    ) { 
      this.nameId =1;
    }

    ngOnInit(){
      this.cargaDays();
    }

    
    
   

    cargaDays() {

      this._peticionesService.getDays().subscribe(
        result => {
          this.names = result.data;
          this.names.sort(function(a,b){
            var nameA=a.first_name.toLowerCase(), nameB=b.first_name.toLowerCase();
            if (nameA < nameB) //sort string ascending
              return -1;
              if (nameA > nameB)
               return 1;
              return 0; //default return value (no sorting)

          })
        }, 
        error => {
          console.log(<any>error);
        }
      );
    }

}
